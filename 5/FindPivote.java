class PivotIndexCalculator {

    static int pivotIndex(int[] nums) {
        int totalSum = 0;
        int leftSum = 0;


        for (int num : nums) {
            totalSum += num;
        }


        for (int i = 0; i < nums.length; ++i) {

            int rightSum = totalSum - leftSum - nums[i];

            if (leftSum == rightSum) {
                return i;
            }

            leftSum += nums[i];
        }

        // No pivot found
        return -1;
    }

    public static void main(String[] args) {
        // Example usage:
        int[] nums1 = {1, 7, 3, 6, 5, 6};
        System.out.println("Example 1: " + pivotIndex(nums1));

        int[] nums2 = {1, 2, 3};
        System.out.println("Example 2: " + pivotIndex(nums2));

        int[] nums3 = {2, 1, -1};
        System.out.println("Example 3: " + pivotIndex(nums3));
    }
}
