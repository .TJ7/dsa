 class EquilibriumPointFinder1{

    static int findEquilibriumPoint(int n, int[] A) {
        int totalSum = 0;
        int leftSum = 0;

        for (int num : A) {
            totalSum += num;
        }


        for (int i = 1; i <= n; ++i) {

            int rightSum = totalSum - leftSum - A[i - 1];

            if (leftSum == rightSum) {
                return i;
            }

            leftSum += A[i - 1];
        }


        return -1;
    }

    public static void main(String[] args) {
        // Example usage:
        int n1 = 5;
        int[] A1 = {1, 3, 5, 2, 2};
        System.out.println("Example 1: " + findEquilibriumPoint(n1, A1));

        int n2 = 1;
        int[] A2 = {1};
        System.out.println("Example 2: " + findEquilibriumPoint(n2, A2));
    }
}
