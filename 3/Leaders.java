import java.util.HashMap;

 class PairsWithSum {
    public int getPairsCount(int[] arr, int n, int k) {
        // HashMap to store the frequency of each element
        HashMap<Integer, Integer> frequencyMap = new HashMap<>();
        int countPairs = 0;

        // Count the frequency of each element in the array
        for (int num : arr) {
            frequencyMap.put(num, frequencyMap.getOrDefault(num, 0) + 1);
        }

        // Check for pairs with sum equal to K
        for (int num : arr) {
            int complement = k - num;

            if (frequencyMap.containsKey(complement)) {
                countPairs += frequencyMap.get(complement);
            }

            // Avoid counting the same pair twice
            if (complement + complement == k) {
                countPairs--;
            }
        }

        // Each pair is counted twice, so divide by 2 to get the actual count
        return countPairs / 2;
    }

    public static void main(String[] args) {
        PairsWithSum pairsFinder = new PairsWithSum();

        // Example usage:
        int[] arr1 = {1, 5, 7, 1};
        System.out.println(pairsFinder.getPairsCount(arr1, 4, 6)); // Output: 2

        int[] arr2 = {1, 1, 1, 1};
        System.out.println(pairsFinder.getPairsCount(arr2, 4, 2)); // Output: 6
    }
}
