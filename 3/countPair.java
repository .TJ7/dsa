import java.util.ArrayList;
import java.util.Collections;

class LeadersInArray {
    public ArrayList<Integer> leaders(int[] A, int n) {
        ArrayList<Integer> result = new ArrayList<>();
        int maxRight = A[n - 1];
        result.add(maxRight);

        for (int i = n - 2; i >= 0; i--) {
            if (A[i] >= maxRight) {
                maxRight = A[i];
                result.add(maxRight);
            }
        }

        // Reverse the result to maintain the order of appearance
        Collections.reverse(result);
        return result;
    }

    public static void main(String[] args) {
        LeadersInArray leaderFinder = new LeadersInArray();

        // Example usage:
        int[] A1 = {16, 17, 4, 3, 5, 2};
        System.out.println(leaderFinder.leaders(A1, 6)); // Output: [17, 5, 2]

        int[] A2 = {1, 2, 3, 4, 0};
        System.out.println(leaderFinder.leaders(A2, 5)); // Output: [4, 0]
    }
}
