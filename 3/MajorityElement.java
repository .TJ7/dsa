 class MajorityElementFinder {
    int majorityElement(int[] nums) {
        // Check for empty array or null input
        if (nums == null || nums.length == 0) {
            return -1; // No majority element
        }

        int candidate = nums[0];
        int count = 1;

        // Apply Moore's Voting Algorithm
        for (int i = 1; i < nums.length; ++i) {
            if (nums[i] == candidate) {
                count++;
            } else {
                count--;
                if (count == 0) {
                    candidate = nums[i];
                    count = 1;
                }
            }
        }

        // Verify the candidate as the majority element
        count = 0;
        for (int num : nums) {
            if (num == candidate) {
                count++;
            }
        }

        return (count > nums.length / 2) ? candidate : -1; // -1 indicates no majority element
    }

    public static void main(String[] args) {
        MajorityElementFinder finder = new MajorityElementFinder();

        // Example usage:
        int[] nums1 = {3, 2, 3};
        System.out.println(finder.majorityElement(nums1)); // Output: 3

        int[] nums2 = {2, 2, 1, 1, 1, 2, 2};
        System.out.println(finder.majorityElement(nums2)); // Output: 2
    }
}
