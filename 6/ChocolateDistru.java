package Arrays;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;

import java.util.Arrays;


class Solution2
{
    public long findMinDiff (ArrayList<Integer> a, int n, int m)
    {
        if (n <= 0 || m <= 0) return 0;

        Collections.sort(a);

        long minDiff = Long.MAX_VALUE;


        for (int i = 0; i + m - 1 < n; ++i) {
            long diff = a.get(i + m - 1) - a.get(i);
            minDiff = Math.min(minDiff, diff);
        }

        return minDiff;
    }

    public static void main(String[] args) {
        Solution2 solution = new Solution2();

        // Example 1
        ArrayList<Integer> example1 = new ArrayList<>(Arrays.asList(3, 4, 1, 9, 56, 7, 9, 12));
        int n1 = 8, m1 = 5;
        long result1 = solution.findMinDiff(example1, n1, m1);
        System.out.println("Example 1 Result: " + result1);

        // Example 2
        ArrayList<Integer> example2 = new ArrayList<>(Arrays.asList(7, 3, 2, 4, 9, 12, 56));
        int n2 = 7, m2 = 3;
        long result2 = solution.findMinDiff(example2, n2, m2);
        System.out.println("Example 2 Result: " + result2);
    }


}