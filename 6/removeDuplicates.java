package Arrays;

class Solution {
    static int removeDuplicates(int[] nums) {

        int d = 1;
        for (int i = 1; i < nums.length; ++i) {
            if (nums[i] != nums[d - 1]) {
                nums[d] = nums[i];
                ++d;
            }
        }
        return d;
    }

    public static void main(String[] args){
        int[]arr=new int[]{0,0,1,1,1,2,2,3,3,4};
        System.out.println(removeDuplicates(arr));

    }

}


