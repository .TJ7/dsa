

import java.util.HashSet;
import java.util.Scanner;

class Solution2 {
     static boolean hasArrayTwoCandidates(int arr[], int n, int x) {
        // code here
        HashSet<Integer> seen = new HashSet<>();

        for (int num : arr) {
            int complement = x - num;

            // If complement is present in HashSet, we found a pair
            if (seen.contains(complement)) {
                return true;
            }

            // Add the current element to the HashSet
            seen.add(num);
        }

        // If no such pair is found, return false
        return false;
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter array size:");
        int size=sc.nextInt();
        int[]arr=new int[size];
        System.out.println("Enter array elements");
        for(int i=0; i<arr.length; i++){
            arr[i]=sc.nextInt();

        }
        System.out.println("enter X:");
        int X=sc.nextInt();
        System.out.println(hasArrayTwoCandidates(arr,arr.length,X));
    }
}