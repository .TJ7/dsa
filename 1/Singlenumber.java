import java.util.HashSet;
import java.util.Set;

/*Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.

You must implement a solution with a linear runtime complexity and use only constant extra space.



Example 1:

Input: nums = [2,2,1]
Output: 1
Example 2:

Input: nums = [4,1,2,1,2]
Output: 4
Example 3:

Input: nums = [1]
Output: 1


Constraints:

1 <= nums.length <= 3 * 104
-3 * 104 <= nums[i] <= 3 * 104
Each element in the array appears twice except for one element which appears only once. */
//Brute force
class Singlenumber {
    static void SingNumber(int[]arr){
        Set <Integer>set=new HashSet<Integer>();

        for(int num:arr){
            if(!set.add(num)){
                set.remove(num);
            }
        }
        for(int num:set){
            System.out.println("singleNumber:"+num);
        }



    }
    public static void main(String[] args) {

        int[]arr=new int[]{4,1,2,1,2};
        SingNumber(arr);


    }
}
