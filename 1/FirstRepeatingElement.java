

import java.util.HashMap;
import java.util.Scanner;

class Solution1 {

    public static int firstRepeated(int[] arr, int n) {

        HashMap<Integer, Integer> hm = new HashMap<>();

        for(int i = 0; i < n; i++) {

            int temp = 0;

            if(hm.containsKey(arr[i]))
                temp = hm.get(arr[i])+1;
            else
                temp = 1;

            hm.put(arr[i], temp);

            // hm.put(arr[i], hm.getOrDefault(arr[i], 0)+1);
        }

        for(int i = 0; i < n; i++) {

            if(hm.get(arr[i]) > 1)
                return i+1;
        }
        return -1;
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter size of an Array");
        int size=sc.nextInt();
        int[]arr=new int[size];
        System.out.println("Enter array elements");
        for(int i=0; i<arr.length;i++){
            arr[i]=sc.nextInt();
        }
        System.out.println(firstRepeated(arr,arr.length));
    }
}
