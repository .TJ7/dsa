class MaxConsecutiveOnes {
    int findMaxConsecutiveOnes(int[] nums) {
        int maxCount = 0;
        int currentCount = 0;

        for (int num : nums) {
            if (num == 1) {
                currentCount++;
                maxCount = Math.max(maxCount, currentCount);
            } else {
                currentCount = 0;
            }
        }

        return maxCount;
    }

    public static void main(String[] args) {
        // Example usage:
        int[] nums1 = {1, 1, 0, 1, 1, 1};
        int[] nums2 = {1, 0, 1, 1, 0, 1};

        MaxConsecutiveOnes solution = new MaxConsecutiveOnes();

        int result1 = solution.findMaxConsecutiveOnes(nums1);
        int result2 = solution.findMaxConsecutiveOnes(nums2);

        System.out.println("Result 1: " + result1);
        System.out.println("Result 2: " + result2);
    }
}
