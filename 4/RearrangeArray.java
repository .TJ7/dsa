class RearrangeArray {
     static void reArrange(int N, int[] arr) {
        int evenIndex = 0;
        int oddIndex = 1;

        while (true) {

            while (evenIndex < N && arr[evenIndex] % 2 == 0) {
                evenIndex += 2;
            }


            while (oddIndex < N && arr[oddIndex] % 2 != 0) {
                oddIndex += 2;
            }


            if (evenIndex < N && oddIndex < N) {
                int temp = arr[evenIndex];
                arr[evenIndex] = arr[oddIndex];
                arr[oddIndex] = temp;
            } else {
                break;
            }
        }
    }

    public static void main(String[] args) {
        int N1 = 6;
        int[] arr1 = {3, 6, 12, 1, 5, 8};
        reArrange(N1, arr1);

        System.out.print("Output 1: ");
        for (int num : arr1) {
            System.out.print(num + " ");
        }

        System.out.println();

        int N2 = 4;
        int[] arr2 = {1, 2, 3, 4};
        reArrange(N2, arr2);

        System.out.print("Output 2: ");
        for (int num : arr2) {
            System.out.print(num + " ");
        }
    }
}
